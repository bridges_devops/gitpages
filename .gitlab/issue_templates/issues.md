----

### Problem to solve
_In a brief statement, summerize the problem we are intending to solve with this issue._


### Beneficiary 
_Who is the recipient(s) of the value this issue provides; a customer, end-user, or buyer. Who benefits from this issue being executed?"_


#### Expected outcome
_What will the user be able to achieve when this issue is executed? For instance, (Users will be able to log in or out of the platform)_


#### Confidence
_How well do we understand the user's problem and their need?_



| Confidence | 
| --- |
| [High/Medium/Low] |  




| Issue | UX Weight |  
| ---------- | --------- | 
| [Issue](link) | `0 - 10` | 
| [Issue](link) | `0 - 10` | 
| [Issue](link) | `0 - 10` | 


#### Ready for developers checklist
The items are self-check suggestions; they could be contributed by designers, product managers, developers. 
* [ ] The stated `Problem to solve` has high confidence 
* [ ] Relevant issues, research, and other background information are linked to the Related issues section
* [ ] The problem statement is clear and has been agreed with the person/s who will be tackling the issue. 
* [ ] The expected outcome is clear and has been agreed with the person/s who will be tackling the issue. 

/label
