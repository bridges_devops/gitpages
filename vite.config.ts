import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  base: 'https://bridges_devops.gitlab.io/gitpages',
  plugins: [react()],
})
